public class ReverseNullExample {
    public static Object callA(Object o) {
        return "hi";
    }
    public static Object callB(Object o) {
        return o.toString();
    }

    public static String testA(Object o) {
        // callB dereferences o, making the later check a bug
        System.out.println(callB(o));
        if( o == null ) {
            System.out.println("It's null");
        }
        return "done";
    }
    
    public static String testB(Object o) {
        Object strB = null;        
        return strB.toString();
    }    
}
